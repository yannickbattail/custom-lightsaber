# Custom blooming sith lightsaber.

Custom sith lightsaber with 6 arms that open when the blade goes out. (blooming like a flower)

![opening sith lightsaber](docs/opening.gif)

It uses collapsible blade by [3dprintingworld](https://thangs.com/3dprintingworld) available
on [thang](https://thangs.com/designer/3dprintingworld/3d-model/Collapsing%20Lightsaber-23595) or
on [thingiverse](https://www.thingiverse.com/thing:3606120).

## Variants

Several variants are available:

- 3 types of handle
- 3 types of pommel

![handles and pommels](docs/handleAndPommel.jpg)

## Functionning

The ring prevent the arms from opening.

When the blade is out it keeps the arms open.

![opening sith lightsaber](docs/openingCut.gif)

## Assembling

Axles for arms are a piece of filament Ø 1.75mm.

## Open source

Sources are available here: https://gitlab.com/yannickbattail/custom-lightsaber

## Customizer variables

### main

- **part** default `all`, values: `all`, `arm`, `saber`, `emitter`, `handle`, `pommel`, `ring`, `fundation`
- **handleType** default `handleType1`, values: `handleType1`, `handleType2`, `handleType3`
- **pommelType** default `pommelType1`, values: `pommelType1`, `pommelType2`, `pommelType3`
- **armStopperSize** default `1` value min `0.5` step `0.1` max `2` *arm stopper size*

- **ingnit** default `true` *ingnit, light up the blade*
- **showBlade** default `true` *show/display the blade*
- **looseCoef** default `0.6` value min `0.1` step `0.1` max `2` *loose coefficient, distance between piece that should*
  move. Adapt it depending on your printer precision.*

### Animation

- **animation_rotation** default `false` *rotating animation*
- **animation_opening** default `false` *opening animation*

### Internal debug

- **debug** default `false` *Debug collision between arms and emitter*
- **cutInQuarter** default `false` *cut the saber in quarter to see the inside*
- **$fn** default `40` *complexity of polygons *
