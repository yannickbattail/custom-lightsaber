# Changelog

## [1.x.y] - Unreleased

### Added

### Changed
- make armStopperSize configurable

### Removed

### Deprecated

### Fixed

## [1.0.2] - 2022-08-05

### Added
- add stopper in arms when opened and closed

### Changed
- reduce emitter height to correspond to the arms
- reduce stop ring height

### Removed

### Deprecated

### Fixed
- fix stopper position after reducing emitter height

## [1.0.1] - 2022-08-02

### Added

- add stopper in the emitter to prevent arms from opening too far

### Changed

### Removed

### Deprecated

### Fixed

## [1.0.0] - 2022-07-29

### Added

### Changed

### Removed

### Deprecated

### Fixed

- fix blade hole. Thanks to 3dprintingworld
