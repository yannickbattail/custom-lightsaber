#!/bin/bash
#set -x

# required image magick
# $ apt-get install imagemagick

fn=50
nb_image=20
image_size=512,512
delay=20 # delay between images en 100th of seconds

mkdir -p ./anim/
#OPENSCAD=openscad
OPENSCAD="openscad-nightly"
parts_list="all arm saber emitter handle pommel ring fundation"
for part in $parts_list
do
    echo generating ./anim/${part}XX.png ...
    $OPENSCAD -q -o ./anim/${part}.png -D "part=\"$part\"" -D "animation_rotation=true" -D "\$fn=${fn}" --animate $nb_image --imgsize $image_size ./lightsaber.scad
    echo generating animation ./anim/${part}.gif ...
    convert -delay $delay -loop 0 ./anim/${part}*.png ./anim/${part}.gif
    echo cleanup animation $part ...
    rm ./anim/${part}*.png
done

echo generating ./anim/openingXX.png ...
$OPENSCAD -q -o ./anim/opening.png -D "animation_rotation=false" -D "animation_opening=true" -D "\$fn=${fn}" --animate $nb_image --imgsize $image_size ./lightsaber.scad
echo generating animation ./anim/opening.gif ...
convert -delay $delay -loop 0 ./anim/opening*.png ./anim/opening.gif
echo cleanup animation openingXX ...
rm ./anim/opening*.png

echo generating ./anim/openingCutXX.png ...
$OPENSCAD -q -o ./anim/openingCut.png -D "animation_rotation=false" -D "animation_opening=true" -D "cutInQuarter=true" -D "\$fn=${fn}" --animate $nb_image --imgsize $image_size ./lightsaber.scad
echo generating animation ./anim/openingCut.gif ...
convert -delay $delay -loop 0 ./anim/openingCut*.png ./anim/openingCut.gif
echo cleanup animation openingCutXX ...
rm ./anim/openingCut*.png

mkdir -p ./docs/
cp ./anim/opening.gif    ./docs/
cp ./anim/openingCut.gif ./docs/

echo done.
