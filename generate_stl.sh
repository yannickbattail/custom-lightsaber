#!/bin/bash
#set -x

fn=50

mkdir -p ./stl/
#OPENSCAD=xvfb-run -a openscad
OPENSCAD="xvfb-run -a openscad-nightly"
parts_list="arm emitter ring fundation" # all saber handle pommel
for part in $parts_list
do
    echo generating ./stl/${part}.stl ...
    $OPENSCAD -q -o ./stl/${part}.stl -D "part=\"$part\"" -D "\$fn=${fn}" --export-format asciistl --enable sort-stl ./lightsaber.scad
done

handleType_list="handleType1 handleType2 handleType3"
for handleType in $handleType_list
do
    echo generating ./stl/${handleType}.stl ...
     $OPENSCAD -q -o ./stl/${handleType}.stl -D "handleType=\"${handleType}\"" -D "part=\"handle\"" -D "\$fn=${fn}" --export-format asciistl --enable sort-stl ./lightsaber.scad
done

pommelType_list="pommelType1 pommelType2 pommelType3"
for pommelType in $pommelType_list
do
    echo generating ./stl/${pommelType}.stl ...
    $OPENSCAD -q -o ./stl/${pommelType}.stl -D "pommelType=\"${pommelType}\"" -D "part=\"pommel\"" -D "\$fn=${fn}" --export-format asciistl --enable sort-stl ./lightsaber.scad
done

echo done.
