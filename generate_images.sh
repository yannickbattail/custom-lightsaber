#!/bin/bash
#set -x

# required image magick
# $ apt-get install imagemagick

fn=50
image_size=1024,1024

mkdir -p ./images/
#OPENSCAD=xvfb-run -a openscad
OPENSCAD="xvfb-run -a openscad-nightly"
parts_list="all arm saber emitter handle pommel ring fundation" 
for part in $parts_list
do
    echo generating ./images/part_${part}.png ...
    $OPENSCAD -q -o ./images/part_${part}.png -D "part=\"${part}\"" -D "\$fn=${fn}" --imgsize $image_size ./lightsaber.scad
done

handleType_list="handleType1 handleType2 handleType3"
for handleType in $handleType_list
do
    echo generating ./images/${handleType}.png ...
    $OPENSCAD -q -o ./images/${handleType}.png -D "handleType=\"${handleType}\"" -D "showBlade=false" -D "ingnit=false" -D "\$fn=${fn}" --imgsize $image_size ./lightsaber.scad
done

pommelType_list="pommelType1 pommelType2 pommelType3"
for pommelType in $pommelType_list
do
    echo generating ./images/${pommelType}.png ...
    $OPENSCAD -q -o ./images/${pommelType}.png -D "pommelType=\"${pommelType}\"" -D "showBlade=false" -D "ingnit=false" -D "\$fn=${fn}" --imgsize $image_size ./lightsaber.scad
done

echo generating ./images/closed.png ...
$OPENSCAD -q -o ./images/closed.png -D "ingnit=false" -D "\$fn=${fn}" --imgsize $image_size ./lightsaber.scad

echo generating montage handleAndPommel.jpg
montage -geometry 256x256+2+2 -tile 3x2 ./images/handleType*.png ./images/pommelType*.png ./images/handleAndPommel.jpg
echo generating montage parts.jpg
montage -geometry 256x256+2+2 ./images/part_*.png ./images/parts.jpg
echo generating montage openclose.jpg
montage -geometry 256x256+2+2 ./images/part_all.png ./images/closed.png ./images/openclose.jpg

mkdir -p ./docs/
cp ./images/handleAndPommel.jpg ./docs/
cp ./images/parts.jpg ./docs/
cp ./images/openclose.jpg ./docs/

echo done.